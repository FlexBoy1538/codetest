import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Comments from '@/components/Comments'
import UserComment from '@/components/UserComment'
Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
	    {
	      path: '/',
	      name: 'Main',
	      component: Main
	    },
	    {
	      path: '/comments',
	      name: 'Comments',
	      component: Comments
	    },
	    {
	      path: '/comment/:id',
	      name: 'UserComment',
	      component: UserComment
	    },
	]
})
